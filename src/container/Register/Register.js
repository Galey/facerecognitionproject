import React, { Component } from 'react';
import Form from '../../components/Form/Form';

class Register extends Component {
	constructor(props) {
		super(props);
		this.state = {
			email: '',
			password: '',
			name: ''
		}
	}

	onNameChange = (event) => {
		this.setState({name: event.target.value});
	}

	onEmailChange = (event) => {
		this.setState({email: event.target.value});
	}

	onPasswordChange = (event) => {
		this.setState({password: event.target.value});
	}

	onSubmitSignIn = () => {
		fetch('https://boiling-meadow-19309.herokuapp.com/register', {
			method: 'post',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({
				name: this.state.name,
				email: this.state.email,
				password: this.state.password
			})
		})
			.then(res => res.json())
			.then(user => {
				if(user.id) {
					this.props.loadUser(user)
					this.props.onRouteChange('home');
				}
			})
	}

	render() {
		const { onRouteChange } = this.props;
		return (
			<Form 
				isSignInForm={false} 
				onEmailChange={this.onEmailChange} 
				onPasswordChange={this.onPasswordChange} 
				onSubmitSignIn={this.onSubmitSignIn}
				onRouteChange={onRouteChange} 
				onNameChange={this.onNameChange}
			/>
		);
	}
	
}

export default Register;