import React, { Component } from 'react';
import Form from '../../components/Form/Form';


class Signin extends Component {
	constructor(props) {
		super(props);
		this.state = {
			signInEmail: '',
			signInPassword: ''
		}
	}

	onEmailChange = (event) => {
		this.setState({signInEmail: event.target.value});
	}

	onPasswordChange = (event) => {
		this.setState({signInPassword: event.target.value});
	}

	onSubmitSignIn = () => {
		fetch('https://boiling-meadow-19309.herokuapp.com/signin', {
			method: 'post',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({
				email: this.state.signInEmail,
				password: this.state.signInPassword
			})
		})
			.then(res => res.json())
			.then(user => {
				if(user.id){
          this.props.loadUser(user);
          this.props.onRouteChange('home');
        }
			})
	}

	render() {
		const { onRouteChange } = this.props;
		return (
			<Form 
				isSignInForm={true}
				onEmailChange={this.onEmailChange} 
				onPasswordChange={this.onPasswordChange} 
				onRouteChange={onRouteChange} 
				onSubmitSignIn={this.onSubmitSignIn} 
			/>
		);
	}	
}

export default Signin;